package com.kafka.producer.api

import java.util.Properties
import org.apache.kafka.clients.producer._

object KafkaProducer  {

  def main(args: Array[String]): Unit = {
    val topicName="producerAPI"
    val key="1"
    val value="simple value"

    writeToKafka(topicName,key,value)
  }

  def writeToKafka(topic: String,key: String,value: String): Unit = {

    //Creating properties object
    val props = new Properties()

    // Defining properties
    props.put("bootstrap.servers", "localhost:9092") // list of brokers. min 2
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")// kafka accepts only  array of bytes. Int/Double etc
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    //step#1 : create producer object
    val producer = new KafkaProducer[String, String](props)
    //step#2 : create producer record object
    val record = new ProducerRecord[String, String](topic, key,value) // also accepts partition and timestamp as an argument
    //step#3 : send the record to producer
    producer.send(record)

    producer.close() // closing is necessary to clean up resources
  }

}
